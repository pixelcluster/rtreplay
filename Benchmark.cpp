/* Copyright (c) 2023 Valve Corporation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "Benchmark.hpp"
#include "RRAParser.hpp"
#include <iostream>
#include <numeric>
#include <string_view>
#include <vulkan/vulkan_core.h>

namespace rtreplay {

    char* filenameFromCommandLine(int argc, char** argv) {
        char* fileName = NULL;
        for (uint32_t i = 1; i < argc; ++i) {
            if (argv[i][0] != '-')
                fileName = argv[i];
        }

        if (!fileName) {
            std::cerr << "No filename specified. Usage:\n"
                      << "RTReplay [-csv] [-v] <rra-trace-file>\n"
                      << "-csv exports data as a CSV\n"
                      << "-v prints more detailed data\n";
            abort();
        }
        return fileName;
    }

    bool commandLineContains(int argc, char** argv, const std::string_view& arg) {

        for (uint32_t i = 1; i < argc; ++i) {
            if (argv[i] == arg)
                return true;
        }
        return false;
    }

    Benchmark::Benchmark(int argc, char** argv, ResultUnit unit, VkBuildAccelerationStructureFlagsKHR extraFlags)
        : m_parseResult(rtreplay::parseRRA(filenameFromCommandLine(argc, argv))),
          m_replayer(m_parseResult, extraFlags | (commandLineContains(argc, argv, "--fast-build")
                                                      ? VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR
                                                      : VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR)) {
        bool exportCSV = false;
        bool exportVerbose = false;
        VkBuildAccelerationStructureFlagsKHR flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
        const char* fileName = nullptr;

        m_exportCSV = commandLineContains(argc, argv, "-csv");
        m_printVerbose = commandLineContains(argc, argv, "-v");
        m_resultUnit = unit;
    }

    void Benchmark::printResults() {
        uint64_t bestPerASResult = ~0U;
        uint64_t averagePerASResult = 0;
        uint64_t worstPerASResult = 0;

        if (m_exportCSV) {
            printf("Average,Worst,Best,PrimCount,VirtualAddress");
            if (m_printVerbose) {
                for (uint32_t i = 0; i < m_results.size(); ++i) {
                    printf(",Run %u", i);
                }
            }
            printf("\n");

            for (uint32_t i = 0; i < m_parseResult.buildInfos.size(); ++i) {
                if (!m_parseResult.buildInfos[i].rangeInfo.primitiveCount)
                    continue;

                uint64_t averageResult = 0;
                uint64_t bestResult = ~0ULL;
                uint64_t worstResult = 0;

                for (auto& result : m_results) {
                    averageResult += result[i];
                    bestResult = std::min(bestResult, result[i]);
                    worstResult = std::max(worstResult, result[i]);
                }
                averageResult /= m_results.size();

                printf("%lu,%lu,%lu,%u,\"0x%lx\"", averageResult, bestResult, worstResult,
                       m_parseResult.buildInfos[i].rangeInfo.primitiveCount, m_parseResult.buildInfos[i].fileVA);
                if (m_printVerbose) {
                    for (auto& result : m_results) {
                        printf(",%lu", result[i]);
                    }
                }
                printf("\n");
            }
        } else {
            if (m_parseResult.buildInfos.empty())
                return;

            if (m_printVerbose)
                printf("Per-AS results:\n");

            for (uint32_t i = 0; i < m_parseResult.buildInfos.size(); ++i) {
                if (!m_parseResult.buildInfos[i].rangeInfo.primitiveCount)
                    continue;

                uint64_t averageResult = 0;
                uint64_t bestResult = ~0ULL;
                uint64_t worstResult = 0;
                for (auto& result : m_results) {
                    averageResult += result[i];
                    bestResult = std::min(bestResult, result[i]);
                    worstResult = std::max(worstResult, result[i]);
                }
                averageResult /= m_results.size();

                uint64_t averageResultPerPrim = 0;
                uint64_t bestResultPerPrim = 0;
                uint64_t worstResultPerPrim = 0;

                averageResultPerPrim = averageResult / m_parseResult.buildInfos[i].rangeInfo.primitiveCount;
                bestResultPerPrim = bestResult / m_parseResult.buildInfos[i].rangeInfo.primitiveCount;
                worstResultPerPrim = worstResult / m_parseResult.buildInfos[i].rangeInfo.primitiveCount;

                if (m_printVerbose) {
                    printf("%s at 0x%lx (%u primitives):\n",
                           m_parseResult.buildInfos[i].geometryInfo.type ==
                                   VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR
                               ? "BLAS"
                               : "TLAS",
                           m_parseResult.buildInfos[i].fileVA, m_parseResult.buildInfos[i].rangeInfo.primitiveCount);
                    /* Averaging and best/worst results only make sense for timing */
                    if (m_resultUnit == ResultUnit::Nanoseconds) {
                        printf("   Average result: ");
                        printUnit(averageResult);
                        printf("   (per primitive: ");
                        printUnit(averageResultPerPrim);
                        printf(")\n");
                        printf("   Best result:    ");
                        printUnit(bestResult);
                        printf("   (per primitive: ");
                        printUnit(bestResultPerPrim);
                        printf(")\n");
                        printf("   Worst result:   ");
                        printUnit(worstResult);
                        printf("   (per primitive: ");
                        printUnit(worstResultPerPrim);
                        printf(")\n");
                    } else {
                        printUnit(averageResult);
                        printf("   (per primitive: ");
                        printUnit(averageResultPerPrim);
                        printf(")\n");
                    }
                }

                bestPerASResult = std::min(bestPerASResult, bestResult);
                averagePerASResult += averageResult;
                worstPerASResult = std::max(worstPerASResult, worstResult);
            }

            averagePerASResult /= m_parseResult.buildInfos.size();

            printf("\nAverages over all ASs:\n");
            if (m_resultUnit == ResultUnit::Nanoseconds) {
                printf("Average result: \n");
                printUnit(averagePerASResult);
                printf("\nBest result:    \n");
                printUnit(bestPerASResult);
                printf("\nWorst result:   \n");
                printUnit(worstPerASResult);
            } else {
                printUnit(averagePerASResult);
            }

            printf("\n\nTotal results:\n");
            uint64_t averageResult = 0;
            uint64_t bestResult = ~0ULL;
            uint64_t worstResult = 0;
            for (auto& result : m_results) {
                uint64_t totalResult = std::accumulate(result.begin(), result.end(), 0ULL);
                averageResult += totalResult;
                bestResult = std::min(bestResult, totalResult);
                worstResult = std::max(worstResult, totalResult);
            }
            averageResult /= m_results.size();
            if (m_resultUnit == ResultUnit::Nanoseconds) {
                printf("Average result: \n");
                printUnit(averageResult);
                printf("\nBest result:    \n");
                printUnit(bestResult);
                printf("\nWorst result:   \n");
                printUnit(worstResult);
            } else {
                printUnit(averageResult);
            }
            printf("\n");
        }
    }

    void Benchmark::printUnit(uint64_t rawValue) {
        switch (m_resultUnit) {
            case ResultUnit::Nanoseconds:
                printf("%10llu ns", (unsigned long long)rawValue);
                /* 1 second */
                if (rawValue > 1000000000ULL)
                    printf("/%9.5f s", static_cast<double>(rawValue) / 1000000000.0);
                else
                    printf("/%9.5f ms", static_cast<double>(rawValue) / 1000000.0);
                break;
            case ResultUnit::Bytes:
                printf("%10llu bytes", (unsigned long long)rawValue);
                /* 1GiB */
                if (rawValue > 0x40000000ULL)
                    printf("/%8.3f GiB", static_cast<double>(rawValue) / static_cast<double>(0x40000000ULL));
                /* 1 MiB */
                else if (rawValue > 0x100000ULL)
                    printf("/%8.3f MiB", static_cast<double>(rawValue) / static_cast<double>(0x100000LL));
                /* 1 KiB */
                else if (rawValue > 1024)
                    printf("/%8.3f KiB", static_cast<double>(rawValue) / 1024.0);
        }
    }
} // namespace rtreplay