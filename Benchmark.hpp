/* Copyright (c) 2023 Valve Corporation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef BENCHMARK_HPP
#define BENCHMARK_HPP

#include "BvhInfoReplayer.hpp"
#include <concepts>
#include <vector>
#include <vulkan/vulkan_core.h>

namespace rtreplay {

    enum class ResultUnit { Nanoseconds, Bytes };

    /*
     * Base class for all benchmarks
     */
    class Benchmark {
      public:
        Benchmark(int argc, char** argv, ResultUnit unit, VkBuildAccelerationStructureFlagsKHR extraFlags);

        virtual void executeBenchmark() = 0;

        void printResults();

      protected:
        std::vector<std::vector<uint64_t>> m_results;
        uint32_t m_numTestRuns = 8;

        rtreplay::RRAParseResult m_parseResult;
        rtreplay::BVHInfoReplayer m_replayer;

      private:
        /* Pretty-prints a raw value in the unit specified in m_resultUnit. */
        void printUnit(uint64_t rawValue);

        ResultUnit m_resultUnit;
        bool m_exportCSV;
        bool m_printVerbose;
    };

    template <std::derived_from<Benchmark> T> void executeAndPrint(int argc, char** argv) {
        T benchmark = T(argc, argv);
        benchmark.executeBenchmark();
        benchmark.printResults();
    }
} // namespace rtreplay

#endif // BENCHMARK_HPP
