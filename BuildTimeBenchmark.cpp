/* Copyright (c) 2023 Valve Corporation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "BuildTimeBenchmark.hpp"
#include <cstdint>
#include <vector>
#include <volk.h>

namespace rtreplay {
    BuildTimeBenchmark::BuildTimeBenchmark(int argc, char** argv) : Benchmark(argc, argv, ResultUnit::Nanoseconds, 0) {
        VkQueryPoolCreateInfo queryPoolCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO,
            .queryType = VK_QUERY_TYPE_TIMESTAMP,
            .queryCount = static_cast<uint32_t>(m_parseResult.buildInfos.size()) + 1,
        };
        vkCreateQueryPool(m_replayer.device(), &queryPoolCreateInfo, nullptr, &m_queryPool);
    }

    void BuildTimeBenchmark::executeBenchmark() {
        for (uint32_t i = 0; i < m_numTestRuns; ++i) {
            /* This vector will contain timestamps in the following order:
             * before build 0
             * before build 1
             * ...
             * before build n
             * after build n
             * */
            std::vector<uint64_t> testRunResults =
                std::vector<uint64_t>(m_replayer.accelerationStructures().size() + 1);
            m_replayer.replay(
                [this](auto& replayer, uint32_t index) {
                    uint32_t resetCount = 1;
                    if (index == replayer.accelerationStructures().size() - 1)
                        resetCount += 1;
                    vkCmdResetQueryPool(replayer.commandBuffer(), m_queryPool, index, resetCount);
                    vkCmdWriteTimestamp(replayer.commandBuffer(), VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, m_queryPool,
                                        index);
                },
                [this](auto& replayer, uint32_t index) {
                    vkCmdPipelineBarrier(replayer.commandBuffer(), VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                         VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 0, nullptr, 0, nullptr, 0, nullptr);
                    if (index == replayer.accelerationStructures().size() - 1)
                        vkCmdWriteTimestamp(replayer.commandBuffer(), VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, m_queryPool,
                                            index + 1);
                });

            uint32_t queryCount = m_replayer.accelerationStructures().size() + 1;
            vkGetQueryPoolResults(m_replayer.device(), m_queryPool, 0, queryCount, queryCount * sizeof(uint64_t),
                                  testRunResults.data(), sizeof(uint64_t), VK_QUERY_RESULT_64_BIT);

            if (m_replayer.queueFamilyProperties().timestampValidBits != 64)
                testRunResults[0] &= (1ULL << m_replayer.queueFamilyProperties().timestampValidBits) - 1;
            for (uint32_t j = 1; j <= m_parseResult.buildInfos.size(); ++j) {
                if (m_replayer.queueFamilyProperties().timestampValidBits != 64)
                    testRunResults[j] &= (1ULL << m_replayer.queueFamilyProperties().timestampValidBits) - 1;
            }

            std::vector<uint64_t> timestampDeltas;
            timestampDeltas.reserve(m_parseResult.buildInfos.size());
            uint64_t maxDelta = 0;
            uint32_t maxIndex = 0;
            for (uint32_t j = 0; j < m_parseResult.buildInfos.size(); ++j) {
                timestampDeltas.push_back(testRunResults[j + 1] - testRunResults[j]);
                if (timestampDeltas[j] > maxDelta) {
                    maxDelta = timestampDeltas[j];
                    maxIndex = j;
                }
            }
            m_results.emplace_back(timestampDeltas);
        }
    }

} // namespace rtreplay

int main(int argc, char** argv) {
    std::cout.imbue(std::locale("C"));
    rtreplay::executeAndPrint<rtreplay::BuildTimeBenchmark>(argc, argv);
}
