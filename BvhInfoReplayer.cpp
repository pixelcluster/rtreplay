/* Copyright (c) 2023 Valve Corporation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#define VK_NO_PROTOTYPES
#include "BvhInfoReplayer.hpp"
#include <bit>
#include <volk.h>

namespace rtreplay {

    void throwOnError(VkResult result) {
        if (result < 0) {
            std::cerr << "ERROR: Vulkan call failed with result " << result << "!\n";
            throw result;
        }
    }

    template <typename HandleType, typename Enumerant, typename... AdditionalParams>
    using AdditionalParameterEnumerationPFN = VkResult(VKAPI_PTR*)(HandleType type, AdditionalParams... params,
                                                                   uint32_t* count, Enumerant* pEnumerants);
    template <typename HandleType, typename Enumerant>
    using EnumerationPFN = VkResult(VKAPI_PTR*)(HandleType type, uint32_t* count, Enumerant* pEnumerants);

    template <typename HandleType, typename Enumerant>
    using VoidEnumerationPFN = void(VKAPI_PTR*)(HandleType type, uint32_t* count, Enumerant* pEnumerants);

    template <typename HandleType, typename Enumerant, typename... AdditionalParams>
    std::vector<Enumerant> enumerate(
        HandleType handle, AdditionalParams... params,
        AdditionalParameterEnumerationPFN<HandleType, Enumerant, AdditionalParams...> pfnEnumerator) {
        std::vector<Enumerant> values;
        uint32_t valueCount;
        VkResult enumerationResult;

        do {
            throwOnError(pfnEnumerator(handle, params..., &valueCount, nullptr));
            values.resize(valueCount);
            enumerationResult = pfnEnumerator(handle, params..., &valueCount, values.data());
        } while (enumerationResult == VK_INCOMPLETE);

        throwOnError(enumerationResult);
        return values;
    }

    template <typename HandleType, typename Enumerant>
    std::vector<Enumerant> enumerate(HandleType handle, EnumerationPFN<HandleType, Enumerant> pfnEnumerator) {
        std::vector<Enumerant> values;
        uint32_t valueCount;
        VkResult enumerationResult;

        do {
            throwOnError(pfnEnumerator(handle, &valueCount, nullptr));
            values = std::vector<Enumerant>(valueCount);
            enumerationResult = pfnEnumerator(handle, &valueCount, values.data());
        } while (enumerationResult == VK_INCOMPLETE);

        throwOnError(enumerationResult);
        return values;
    }

    template <typename HandleType, typename Enumerant>
    std::vector<Enumerant> enumerate(HandleType handle, VoidEnumerationPFN<HandleType, Enumerant> pfnEnumerator) {
        std::vector<Enumerant> values;
        uint32_t valueCount;

        pfnEnumerator(handle, &valueCount, nullptr);
        values = std::vector<Enumerant>(valueCount);
        pfnEnumerator(handle, &valueCount, values.data());

        return values;
    }

    uint32_t BVHInfoReplayer::bestTypeIndex(VkMemoryPropertyFlags required, VkMemoryPropertyFlags preferred,
                                            VkMemoryRequirements requirements) {
        uint32_t bestMatchingTypeIndex = ~0U;
        size_t bestNumMatchingCapabilities = 0;
        size_t bestNumUnrelatedCapabilities = ~0U;

        for (uint32_t typeIndex = 0; typeIndex < m_memoryProperties.memoryTypeCount; ++typeIndex) {
            auto& type = m_memoryProperties.memoryTypes[typeIndex];
            if ((type.propertyFlags & required) != required ||
                /* Ignore device coherent memory. It requires extra features to be used
                 * (and ruins performance) */
                type.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_COHERENT_BIT_AMD) {
                ++typeIndex;
                continue;
            }

            size_t numMatchingCapabilities = std::popcount(preferred & type.propertyFlags);
            size_t numUnrelatedCapabilities = std::popcount((~preferred) & type.propertyFlags);
            if ((numMatchingCapabilities > bestNumMatchingCapabilities ||
                 (numMatchingCapabilities == bestNumMatchingCapabilities &&
                  numUnrelatedCapabilities < bestNumUnrelatedCapabilities)) &&
                ((1U << typeIndex) & requirements.memoryTypeBits)) {
                bestMatchingTypeIndex = typeIndex;
                bestNumMatchingCapabilities = numMatchingCapabilities;
                bestNumUnrelatedCapabilities = numUnrelatedCapabilities;
            }
            ++typeIndex;
        }

        return bestMatchingTypeIndex;
    }

    MemoryBlob BVHInfoReplayer::allocateBlob(VkMemoryPropertyFlags required, VkMemoryPropertyFlags preferred,
                                             VkDeviceSize size) {
        MemoryBlob blob;
        VkBufferCreateInfo bufferCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .size = size,
            .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT |
                     VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                     VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR |
                     VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        };
        throwOnError(vkCreateBuffer(m_device, &bufferCreateInfo, nullptr, &blob.buffer));

        VkMemoryRequirements requirements;
        vkGetBufferMemoryRequirements(m_device, blob.buffer, &requirements);

        uint32_t bestMatchingTypeIndex = bestTypeIndex(required, preferred, requirements);
        if (bestMatchingTypeIndex == ~0U)
            throwOnError(VK_ERROR_OUT_OF_DEVICE_MEMORY);

        VkMemoryAllocateFlagsInfo flagsInfo = {
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO,
            .flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT,
        };

        VkMemoryAllocateInfo allocInfo = {
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .pNext = &flagsInfo,
            .allocationSize = requirements.size,
            .memoryTypeIndex = bestMatchingTypeIndex,
        };
        throwOnError(vkAllocateMemory(m_device, &allocInfo, nullptr, &blob.mem));

        vkBindBufferMemory(m_device, blob.buffer, blob.mem, 0);

        VkBufferDeviceAddressInfo deviceAddressInfo = {
            .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
            .buffer = blob.buffer,
        };
        blob.va = vkGetBufferDeviceAddress(m_device, &deviceAddressInfo);

        return blob;
    }

    BVHInfoReplayer::BVHInfoReplayer(RRAParseResult& parseResult, VkBuildAccelerationStructureFlagsKHR buildFlags)
        : m_parseResult(parseResult) {

        throwOnError(volkInitialize());

        const char* enabledInstanceExtension = VK_KHR_SURFACE_EXTENSION_NAME;
        VkApplicationInfo appInfo = {
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pApplicationName = "RRA BVH build replayer",
            .apiVersion = VK_API_VERSION_1_2,
        };
        VkInstanceCreateInfo instanceCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pApplicationInfo = &appInfo,
            .enabledExtensionCount = 1,
            .ppEnabledExtensionNames = &enabledInstanceExtension,
        };
        throwOnError(vkCreateInstance(&instanceCreateInfo, nullptr, &m_instance));
        volkLoadInstanceOnly(m_instance);

        std::vector<VkPhysicalDevice> devices = enumerate(m_instance, vkEnumeratePhysicalDevices);

        m_physicalDevice = VK_NULL_HANDLE;
        for (auto& currentDevice : devices) {
            std::vector<VkExtensionProperties> extensions =
                enumerate<VkPhysicalDevice, VkExtensionProperties, const char*>(currentDevice, nullptr,
                                                                                vkEnumerateDeviceExtensionProperties);
            bool isCompatible = false;
            for (auto& extension : extensions) {
                if (!strcmp(extension.extensionName, VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME)) {
                    isCompatible = true;
                    break;
                }
            }

            if (isCompatible) {
                m_physicalDevice = currentDevice;
                break;
            }
        }

        if (!m_physicalDevice) {
            std::cerr << "ERROR: No compatible physical devices found!\n";
            throw 1;
        }

        const char* enabledExtensions[4] = {
            VK_KHR_SWAPCHAIN_EXTENSION_NAME,
            VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME,
            VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME,
            VK_KHR_RAY_QUERY_EXTENSION_NAME,
        };

        std::vector<VkQueueFamilyProperties> queueFamilyProperties =
            enumerate(m_physicalDevice, vkGetPhysicalDeviceQueueFamilyProperties);
        m_queueFamilyIndex = 0;
        uint32_t currentQueueFamilyIndex = 0;
        for (auto& property : queueFamilyProperties) {
            if (property.queueFlags & VK_QUEUE_COMPUTE_BIT) {
                m_queueFamilyIndex = currentQueueFamilyIndex;
                /* Prefer compute-only queues, we don't need graphics */
                if ((property.queueFlags & VK_QUEUE_GRAPHICS_BIT) == 0)
                    break;
            }
            ++currentQueueFamilyIndex;
        }
        m_queueFamilyProperties = queueFamilyProperties[m_queueFamilyIndex];

        vkGetPhysicalDeviceProperties(m_physicalDevice, &m_physicalDeviceProperties);
        vkGetPhysicalDeviceMemoryProperties(m_physicalDevice, &m_memoryProperties);

        float queuePriority = 1.0f;
        VkDeviceQueueCreateInfo queueCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = m_queueFamilyIndex,
            .queueCount = 1,
            .pQueuePriorities = &queuePriority,
        };

        VkPhysicalDeviceAccelerationStructureFeaturesKHR accelerationStructureFeatures = {
            .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR,
            .accelerationStructure = true,
        };
        VkPhysicalDeviceBufferDeviceAddressFeatures bdaFeatures = {
            .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES,
            .pNext = &accelerationStructureFeatures,
            .bufferDeviceAddress = true,
        };
        VkPhysicalDeviceRayQueryFeaturesKHR rqFeatures = {
            .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR,
            .pNext = &bdaFeatures,
            .rayQuery = true,
        };

        VkPhysicalDeviceFeatures2 enabledFeatures = {
            .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
            .pNext = &rqFeatures,
        };

        VkDeviceCreateInfo deviceCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
            .pNext = &enabledFeatures,
            .queueCreateInfoCount = 1,
            .pQueueCreateInfos = &queueCreateInfo,
            .enabledExtensionCount = 4,
            .ppEnabledExtensionNames = enabledExtensions,
        };
        throwOnError(vkCreateDevice(m_physicalDevice, &deviceCreateInfo, nullptr, &m_device));
        volkLoadDevice(m_device);
        vkGetDeviceQueue(m_device, m_queueFamilyIndex, 0, &m_queue);

        VkCommandPoolCreateInfo commandPoolCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
            .queueFamilyIndex = m_queueFamilyIndex,
        };
        throwOnError(vkCreateCommandPool(m_device, &commandPoolCreateInfo, nullptr, &m_commandPool));

        VkCommandBufferAllocateInfo commandBufferAllocateInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .commandPool = m_commandPool,
            .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            .commandBufferCount = 1,
        };
        throwOnError(vkAllocateCommandBuffers(m_device, &commandBufferAllocateInfo, &m_commandBuffer));

        std::vector<VkDeviceAddress> blasVAs;
        blasVAs.reserve(m_parseResult.blasCount);

        std::vector<MemoryBlob> stagingBlobs;
        VkDeviceSize maxScratchSize = 0;

        VkCommandBufferBeginInfo beginInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        };
        throwOnError(vkBeginCommandBuffer(m_commandBuffer, &beginInfo));

        for (auto& accelerationStructure : m_parseResult.buildInfos) {
            accelerationStructure.geometryInfo.pGeometries = &accelerationStructure.geometry;
            accelerationStructure.geometryInfo.flags = buildFlags;

            VkAccelerationStructureBuildSizesInfoKHR buildSizesInfo = {
                .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR,
            };
            uint32_t maxPrimCount = accelerationStructure.rangeInfo.primitiveCount;

            if (!maxPrimCount) {
                m_accelerationStructures.push_back({});
                continue;
            }

            vkGetAccelerationStructureBuildSizesKHR(m_device, VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
                                                    &accelerationStructure.geometryInfo, &maxPrimCount,
                                                    &buildSizesInfo);

            maxScratchSize =
                std::max(buildSizesInfo.buildScratchSize, std::max(buildSizesInfo.updateScratchSize, maxScratchSize));

            AccelerationStructureData data;
            data.storage =
                allocateBlob(0, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, buildSizesInfo.accelerationStructureSize);
            VkAccelerationStructureCreateInfoKHR accelerationStructureCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR,
                .buffer = data.storage.buffer,
                .offset = 0,
                .size = buildSizesInfo.accelerationStructureSize,
                .type = accelerationStructure.geometryInfo.type,
            };
            throwOnError(
                vkCreateAccelerationStructureKHR(m_device, &accelerationStructureCreateInfo, nullptr, &data.as));

            accelerationStructure.geometryInfo.srcAccelerationStructure = data.as;
            accelerationStructure.geometryInfo.dstAccelerationStructure = data.as;

            if (accelerationStructure.geometryInfo.type == VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR) {
                VkAccelerationStructureDeviceAddressInfoKHR deviceAddressInfo = {
                    .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR,
                    .accelerationStructure = data.as,
                };
                blasVAs.push_back(vkGetAccelerationStructureDeviceAddressKHR(m_device, &deviceAddressInfo));
            }

            VkBufferCopy copy = {
                .size = VK_WHOLE_SIZE,
            };
            switch (accelerationStructure.geometry.geometryType) {
                case VK_GEOMETRY_TYPE_TRIANGLES_KHR: {
                    data.vertexData =
                        allocateBlob(0, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, maxPrimCount * 9 * sizeof(float));

                    accelerationStructure.geometry.geometry.triangles.vertexData.deviceAddress = data.vertexData.va;

                    MemoryBlob vertexStagingBlob =
                        allocateBlob(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, VK_MEMORY_PROPERTY_HOST_CACHED_BIT,
                                     maxPrimCount * 9 * sizeof(float));

                    void* vertexMap;
                    throwOnError(vkMapMemory(m_device, vertexStagingBlob.mem, 0, VK_WHOLE_SIZE, 0, &vertexMap));

                    std::memcpy(vertexMap, accelerationStructure.vertexData, maxPrimCount * 9 * sizeof(float));

                    delete[] accelerationStructure.vertexData;

                    copy.size = maxPrimCount * 9 * sizeof(float);
                    vkCmdCopyBuffer(m_commandBuffer, vertexStagingBlob.buffer, data.vertexData.buffer, 1, &copy);
                    copy.size = maxPrimCount * 3 * sizeof(uint32_t);

                    stagingBlobs.push_back(vertexStagingBlob);
                    break;
                }
                case VK_GEOMETRY_TYPE_AABBS_KHR: {
                    data.aabbData =
                        allocateBlob(0, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, maxPrimCount * 6 * sizeof(float));
                    MemoryBlob aabbStagingBlob =
                        allocateBlob(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, VK_MEMORY_PROPERTY_HOST_CACHED_BIT,
                                     maxPrimCount * 6 * sizeof(float));

                    accelerationStructure.geometry.geometry.aabbs.data.deviceAddress = data.aabbData.va;

                    void* aabbMap;
                    throwOnError(vkMapMemory(m_device, aabbStagingBlob.mem, 0, VK_WHOLE_SIZE, 0, &aabbMap));

                    std::memcpy(aabbMap, accelerationStructure.vertexData, maxPrimCount * 6 * sizeof(float));

                    delete[] accelerationStructure.vertexData;

                    copy.size = maxPrimCount * 6 * sizeof(float);
                    vkCmdCopyBuffer(m_commandBuffer, aabbStagingBlob.buffer, data.aabbData.buffer, 1, &copy);
                    stagingBlobs.push_back(aabbStagingBlob);
                    break;
                }
                case VK_GEOMETRY_TYPE_INSTANCES_KHR: {
                    data.instanceData = allocateBlob(0, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                                                     maxPrimCount * sizeof(VkAccelerationStructureInstanceKHR));
                    MemoryBlob instanceStagingBlob =
                        allocateBlob(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, VK_MEMORY_PROPERTY_HOST_CACHED_BIT,
                                     maxPrimCount * sizeof(VkAccelerationStructureInstanceKHR));

                    accelerationStructure.geometry.geometry.instances.data.deviceAddress = data.instanceData.va;

                    for (uint32_t i = 0; i < maxPrimCount; ++i) {
                        accelerationStructure.instanceData[i].accelerationStructureReference =
                            blasVAs[accelerationStructure.instanceData[i].accelerationStructureReference];
                    }

                    void* instanceMap;
                    throwOnError(vkMapMemory(m_device, instanceStagingBlob.mem, 0, VK_WHOLE_SIZE, 0, &instanceMap));

                    std::memcpy(instanceMap, accelerationStructure.instanceData,
                                maxPrimCount * sizeof(VkAccelerationStructureInstanceKHR));

                    delete[] accelerationStructure.instanceData;

                    copy.size = maxPrimCount * sizeof(VkAccelerationStructureInstanceKHR);
                    vkCmdCopyBuffer(m_commandBuffer, instanceStagingBlob.buffer, data.instanceData.buffer, 1, &copy);
                    stagingBlobs.push_back(instanceStagingBlob);
                    break;
                }
                default:
                    break;
            }
            m_accelerationStructures.push_back(data);
        }

        m_scratchBlob = allocateBlob(0, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, maxScratchSize);

        throwOnError(vkEndCommandBuffer(m_commandBuffer));

        VkSubmitInfo submitInfo = {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .commandBufferCount = 1,
            .pCommandBuffers = &m_commandBuffer,
        };
        throwOnError(vkQueueSubmit(m_queue, 1, &submitInfo, VK_NULL_HANDLE));

        throwOnError(vkQueueWaitIdle(m_queue));

        for (auto& blob : stagingBlobs) {
            vkDestroyBuffer(m_device, blob.buffer, nullptr);
            vkFreeMemory(m_device, blob.mem, nullptr);
        }

        vkResetCommandPool(m_device, m_commandPool, 0);
    }

    VkResult BVHInfoReplayer::replay(const std::function<void(BVHInfoReplayer&, uint32_t)>& before_build,
                                     const std::function<void(BVHInfoReplayer&, uint32_t)>& after_build) {
        VkCommandBufferBeginInfo beginInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        };
        throwOnError(vkBeginCommandBuffer(m_commandBuffer, &beginInfo));

        for (uint32_t i = 0; i < m_accelerationStructures.size(); ++i) {
            m_parseResult.buildInfos[i].geometryInfo.scratchData.deviceAddress = m_scratchBlob.va;

            VkMemoryBarrier barrier = {
                .sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER,
                .srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT | VK_ACCESS_MEMORY_READ_BIT,
                .dstAccessMask = VK_ACCESS_MEMORY_WRITE_BIT | VK_ACCESS_MEMORY_READ_BIT,
            };
            vkCmdPipelineBarrier(m_commandBuffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                 VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 1, &barrier, 0, nullptr, 0, nullptr);

            before_build(*this, i);

            auto rangePtr = &m_parseResult.buildInfos[i].rangeInfo;
            if (rangePtr->primitiveCount)
                vkCmdBuildAccelerationStructuresKHR(m_commandBuffer, 1, &m_parseResult.buildInfos[i].geometryInfo,
                                                    &rangePtr);

            after_build(*this, i);
        }

        throwOnError(vkEndCommandBuffer(m_commandBuffer));

        VkSubmitInfo submitInfo = {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .commandBufferCount = 1,
            .pCommandBuffers = &m_commandBuffer,
        };
        throwOnError(vkQueueSubmit(m_queue, 1, &submitInfo, VK_NULL_HANDLE));

        throwOnError(vkQueueWaitIdle(m_queue));
        vkResetCommandPool(m_device, m_commandPool, 0);
        return VK_SUCCESS;
    }
} // namespace rtreplay