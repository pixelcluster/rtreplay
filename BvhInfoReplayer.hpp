/* Copyright (c) 2023 Valve Corporation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef BVHINFOREPLAYER_HPP
#define BVHINFOREPLAYER_HPP

#include "RRAParser.hpp"
#include <functional>

namespace rtreplay {

    struct MemoryBlob {
        VkDeviceMemory mem;
        VkBuffer buffer;
        VkDeviceAddress va;
    };

    struct AccelerationStructureData {
        MemoryBlob storage;
        VkAccelerationStructureKHR as;

        union {
            MemoryBlob vertexData;
            MemoryBlob aabbData;
            MemoryBlob instanceData;
        };
    };

    class BVHInfoReplayer {
      public:
        BVHInfoReplayer(RRAParseResult& parseResult, VkBuildAccelerationStructureFlagsKHR buildFlags);
        BVHInfoReplayer(const BVHInfoReplayer&) = delete;
        BVHInfoReplayer(BVHInfoReplayer&&) = default;

        VkResult replay(const std::function<void(BVHInfoReplayer& replayer, uint32_t bvhIndex)>& before_build,
                        const std::function<void(BVHInfoReplayer& replayer, uint32_t bvhIndex)>& after_build);

        VkInstance instance() const { return m_instance; }
        VkPhysicalDevice physicalDevice() const { return m_physicalDevice; }
        const VkPhysicalDeviceMemoryProperties& memoryProperties() const { return m_memoryProperties; }
        const VkPhysicalDeviceProperties& physicalDeviceProperties() const { return m_physicalDeviceProperties; }
        const VkQueueFamilyProperties& queueFamilyProperties() const { return m_queueFamilyProperties; }
        VkDevice device() const { return m_device; }
        VkQueue queue() const { return m_queue; }
        uint32_t queueFamilyIndex() const { return m_queueFamilyIndex; }
        VkCommandPool commandPool() const { return m_commandPool; }
        VkCommandBuffer commandBuffer() const { return m_commandBuffer; }
        const std::vector<AccelerationStructureData>& accelerationStructures() const {
            return m_accelerationStructures;
        }

      private:
        MemoryBlob allocateBlob(VkMemoryPropertyFlags required, VkMemoryPropertyFlags preferred, VkDeviceSize size);
        uint32_t bestTypeIndex(VkMemoryPropertyFlags required, VkMemoryPropertyFlags preferred,
                               VkMemoryRequirements requirements);

        RRAParseResult& m_parseResult;
        VkInstance m_instance;
        VkPhysicalDevice m_physicalDevice;
        VkPhysicalDeviceProperties m_physicalDeviceProperties;
        VkQueueFamilyProperties m_queueFamilyProperties;
        VkPhysicalDeviceMemoryProperties m_memoryProperties;
        VkDevice m_device;
        VkQueue m_queue;
        uint32_t m_queueFamilyIndex;

        VkCommandPool m_commandPool;
        VkCommandBuffer m_commandBuffer;

        std::vector<AccelerationStructureData> m_accelerationStructures;
        MemoryBlob m_scratchBlob;
    };

} // namespace rtreplay

#endif // BVHINFOREPLAYER_HPP
