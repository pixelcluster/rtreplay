cmake_minimum_required(VERSION 3.15)
project(RTReplay)

set(CMAKE_CXX_STANDARD 20)

find_package(Vulkan REQUIRED)

add_subdirectory(rdf)
add_subdirectory(volk)

add_library(Replayer BvhInfoReplayer.cpp Benchmark.cpp Benchmark.hpp)
target_link_libraries(Replayer volk amdrdf)

add_executable(TimeBench Benchmark.cpp Benchmark.hpp BuildTimeBenchmark.cpp BuildTimeBenchmark.hpp)
target_link_libraries(TimeBench Replayer)

add_executable(CompSizeBench Benchmark.cpp Benchmark.hpp CompactedSizeBenchmark.cpp CompactedSizeBenchmark.hpp)
target_link_libraries(CompSizeBench Replayer)