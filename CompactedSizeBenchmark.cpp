/* Copyright (c) 2023 Valve Corporation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "CompactedSizeBenchmark.hpp"
#include <cstdint>
#include <vector>
#include <volk.h>
#include <vulkan/vulkan_core.h>

namespace rtreplay {
    CompactedSizeBenchmark::CompactedSizeBenchmark(int argc, char** argv)
        : Benchmark(argc, argv, ResultUnit::Bytes, VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_COMPACTION_BIT_KHR) {
        VkQueryPoolCreateInfo queryPoolCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO,
            .queryType = VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_KHR,
            .queryCount = static_cast<uint32_t>(m_parseResult.buildInfos.size()),
        };
        vkCreateQueryPool(m_replayer.device(), &queryPoolCreateInfo, nullptr, &m_queryPool);
        m_numTestRuns = 1;
    }

    void CompactedSizeBenchmark::executeBenchmark() {
        std::vector<uint64_t> testResults = std::vector<uint64_t>(m_replayer.accelerationStructures().size());
        m_replayer.replay(
            [this](auto& replayer, uint32_t index) {
                vkCmdResetQueryPool(replayer.commandBuffer(), m_queryPool, index, 1);
            },
            [this](auto& replayer, uint32_t index) {
                vkCmdPipelineBarrier(replayer.commandBuffer(), VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                     VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, 0, 0, nullptr, 0, nullptr, 0, nullptr);

                if (m_replayer.accelerationStructures()[index].as) {
                    VkAccelerationStructureKHR structure = m_replayer.accelerationStructures()[index].as;
                    vkCmdWriteAccelerationStructuresPropertiesKHR(
                        replayer.commandBuffer(), 1, &structure,
                        VK_QUERY_TYPE_ACCELERATION_STRUCTURE_COMPACTED_SIZE_KHR, m_queryPool, index);
                }
            });

        uint32_t queryCount = m_replayer.accelerationStructures().size() + 1;
        vkGetQueryPoolResults(m_replayer.device(), m_queryPool, 0, queryCount, queryCount * sizeof(uint64_t),
                              testResults.data(), sizeof(uint64_t), VK_QUERY_RESULT_64_BIT);

        m_results.emplace_back(testResults);
    }
} // namespace rtreplay

int main(int argc, char** argv) {
    std::cout.imbue(std::locale("C"));
    rtreplay::executeAndPrint<rtreplay::CompactedSizeBenchmark>(argc, argv);
}
