/* Copyright (c) 2023 Valve Corporation.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef RRAPARSER_HPP
#define RRAPARSER_HPP

#define VK_NO_PROTOTYPES
#include "rdf/rdf/inc/amdrdf.h"
#include <iostream>
#include <string_view>
#include <vulkan/vulkan.h>

namespace rtreplay {

    struct BVHBuildInfo {
        VkAccelerationStructureBuildGeometryInfoKHR geometryInfo;
        VkAccelerationStructureBuildRangeInfoKHR rangeInfo;
        VkAccelerationStructureGeometryKHR geometry;

        uint64_t fileVA;

        union {
            float* vertexData;
            /* For instances, the BLAS reference cannot be determined at parse time.
             * Instead, the reference is set to the index of the BLAS so the VA can
             * be looked up at a later point. */
            VkAccelerationStructureInstanceKHR* instanceData;
        };
    };

    struct RRAParseResult {
        /* BLASs are guaranteed to be in front of TLASs */
        std::vector<BVHBuildInfo> buildInfos;
        uint32_t blasCount;
    };

    enum class RRABvhType {
        TLAS,
        BLAS,
    };

    struct RRAAccelStructChunkHeader {
        uint32_t virtual_address[2];
        uint32_t metadata_offset;
        uint32_t metadata_size;
        uint32_t header_offset;
        uint32_t header_size;
        RRABvhType bvh_type;
    };

    struct RRAAccelStructPostBuildInfo {
        uint32_t bvh_type : 1;
        uint32_t reserved1 : 5;
        uint32_t tri_compression_mode : 2;
        uint32_t fp16_interior_mode : 2;
        uint32_t reserved2 : 6;
        uint32_t build_flags : 16;
    };

    struct RRAAccelStructHeader {
        struct RRAAccelStructPostBuildInfo post_build_info;
        /*
         * Size of the internal acceleration structure metadata in the
         * proprietary drivers. Seems to always be 128.
         */
        uint32_t metadata_size;
        uint32_t file_size;
        uint32_t primitive_count;
        uint32_t active_primitive_count;
        uint32_t unused1;
        uint32_t geometry_description_count;
        VkGeometryTypeKHR geometry_type;
        uint32_t internal_nodes_offset;
        uint32_t leaf_nodes_offset;
        uint32_t geometry_infos_offset;
        uint32_t leaf_ids_offset;
        uint32_t interior_fp32_node_count;
        uint32_t interior_fp16_node_count;
        uint32_t leaf_node_count;
        uint32_t rt_driver_interface_version;
        uint64_t unused2;
        uint32_t half_fp32_node_count;
        char unused3[44];
    };

    struct RRAInstanceNode {
        float wto_matrix[12];
        uint32_t custom_instance_id : 24;
        uint32_t mask : 8;
        uint32_t sbt_offset : 24;
        uint32_t instance_flags : 8;
        uint64_t blas_va : 54;
        uint64_t hw_instance_flags : 10;
        uint32_t instance_id;
        uint32_t unused1;
        uint32_t blas_metadata_size;
        uint32_t unused2;
        float otw_matrix[12];
    };

    struct RRAAABBNode {
        float aabb[2][3];
        uint32_t unused1[6];
        uint32_t geometry_id : 28;
        uint32_t flags : 4;
        uint32_t primitive_id;
        uint32_t unused[2];
    };

    struct RRATriangleNode {
        float coords[3][3];
        uint32_t reserved[3];
        uint32_t geometry_id : 28;
        uint32_t flags : 4;
        uint32_t triangle_id;
        uint32_t reserved2;
        uint32_t id;
    };

#define RDF_CHUNK_FOREACH(file, identifier, index)                                                                     \
    {                                                                                                                  \
        rdfChunkFileIterator* foreach_iterator;                                                                        \
        int foreach_atEnd;                                                                                             \
        rdfChunkFileCreateChunkIterator(file, &foreach_iterator);                                                      \
        rdfChunkFileIteratorIsAtEnd(foreach_iterator, &foreach_atEnd);                                                 \
        while (!foreach_atEnd) {                                                                                       \
            int index;                                                                                                 \
            char identifier[RDF_IDENTIFIER_SIZE];                                                                      \
            rdfChunkFileIteratorGetChunkIdentifier(foreach_iterator, identifier);                                      \
            rdfChunkFileIteratorGetChunkIndex(foreach_iterator, &index);

#define RDF_CHUNK_FOREACH_END                                                                                          \
    rdfChunkFileIteratorAdvance(foreach_iterator);                                                                     \
    rdfChunkFileIteratorIsAtEnd(foreach_iterator, &foreach_atEnd);                                                     \
    }                                                                                                                  \
    }

    static inline RRAParseResult parseRRA(const std::string_view& filename) {
        RRAParseResult result = {};

        /* Maps VAs/IDs of BLASs to their respective indices in the BVH build info array. */
        std::unordered_map<uint64_t, uint32_t> id_index_map;

        rdfChunkFile* file;
        rdfChunkFileIterator* iterator;
        if (rdfChunkFileOpenFile(filename.data(), &file) != rdfResultOk) {
            std::cerr << "ERROR: Invalid RRA file (Opening failed)!\n";
            return {};
        }

        uint32_t blas_index = 0;

        RDF_CHUNK_FOREACH (file, identifier, index)
            if (!strcmp(identifier, "RawAccelStruc")) {
                int64_t header_size;
                rdfChunkFileGetChunkDataSize(file, identifier, index, &header_size);

                char* header_data = new char[header_size];
                rdfChunkFileReadChunkHeader(file, identifier, index, header_data);
                auto metadata = reinterpret_cast<RRAAccelStructChunkHeader*>(header_data);

                auto virtual_address = static_cast<uint64_t>(metadata->virtual_address[0]);
                virtual_address |= static_cast<uint64_t>(metadata->virtual_address[1]) << 32;

                if (metadata->bvh_type == RRABvhType::BLAS) {
                    id_index_map.insert(std::pair(virtual_address, blas_index++));
                    ++result.blasCount;
                }
                delete[] header_data;
            }
        RDF_CHUNK_FOREACH_END

        RDF_CHUNK_FOREACH (file, identifier, index)
            if (!strcmp(identifier, "RawAccelStruc")) {
                int64_t header_size;
                rdfChunkFileGetChunkDataSize(file, identifier, index, &header_size);

                char* header_data = new char[header_size];
                rdfChunkFileReadChunkHeader(file, identifier, index, header_data);
                auto metadata = reinterpret_cast<RRAAccelStructChunkHeader*>(header_data);

                auto virtual_address = static_cast<uint64_t>(metadata->virtual_address[0]);
                virtual_address |= static_cast<uint64_t>(metadata->virtual_address[1]) << 32;

                int64_t data_size;
                rdfChunkFileGetChunkDataSize(file, identifier, index, &data_size);

                char* data = new char[data_size];
                rdfChunkFileReadChunkData(file, identifier, index, data);

                char* as_header_data = data + metadata->header_offset;
                if (metadata->header_offset > data_size) {
                    std::cerr << "ERROR: Invalid RRA file (Chunk header out of bounds)!\n";
                    return {};
                }
                auto header = reinterpret_cast<RRAAccelStructHeader*>(as_header_data);

                char* leaf_node_data = as_header_data + header->leaf_nodes_offset - 8;
                char* geometry_description_data = as_header_data + header->geometry_infos_offset;

                uint32_t remaining_size = data_size - metadata->header_offset;
                if (header->leaf_ids_offset > remaining_size || header->geometry_infos_offset > remaining_size) {
                    std::cerr << "ERROR: Invalid RRA file (Acceleration structure out of bounds)!\n";
                    return {};
                }

                float* vertexData = nullptr;
                VkAccelerationStructureInstanceKHR* instanceData = nullptr;
                uint32_t primitive_count;

                VkAccelerationStructureGeometryKHR geometry = {
                    .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR,
                    .geometryType = header->geometry_type,
                };

                switch (header->geometry_type) {
                    case VK_GEOMETRY_TYPE_TRIANGLES_KHR: {
                        uint32_t node_count = (geometry_description_data - leaf_node_data) / sizeof(RRATriangleNode);

                        auto nodes = reinterpret_cast<RRATriangleNode*>(leaf_node_data);

                        primitive_count = node_count;

                        vertexData = new float[primitive_count * 9];

                        uint32_t write_idx = 0;
                        for (uint32_t i = 0; i < node_count; ++i) {
                            for (auto& coords : nodes[i].coords)
                                for (auto coord : coords)
                                    vertexData[write_idx++] = coord;
                        }
                        geometry.geometry.triangles = {
                            .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR,
                            .vertexFormat = VK_FORMAT_R32G32B32_SFLOAT,
                            .vertexStride = 12,
                            .indexType = VK_INDEX_TYPE_NONE_KHR,
                        };
                        break;
                    }
                    case VK_GEOMETRY_TYPE_AABBS_KHR: {
                        primitive_count = (geometry_description_data - leaf_node_data) / sizeof(RRAAABBNode);

                        auto nodes = reinterpret_cast<RRAAABBNode*>(leaf_node_data);

                        vertexData = new float[primitive_count * 6];
                        uint32_t write_idx = 0;
                        for (uint32_t i = 0; i < primitive_count; ++i) {
                            for (auto& coords : nodes[i].aabb)
                                for (auto coord : coords)
                                    vertexData[write_idx++] = coord;
                        }
                        geometry.geometry.aabbs = {
                            .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_AABBS_DATA_KHR,
                            .stride = sizeof(float) * 6,
                        };
                        break;
                    }
                    case VK_GEOMETRY_TYPE_INSTANCES_KHR: {
                        primitive_count = (geometry_description_data - leaf_node_data) / sizeof(RRAInstanceNode);

                        auto nodes = reinterpret_cast<RRAInstanceNode*>(leaf_node_data);

                        instanceData = new VkAccelerationStructureInstanceKHR[primitive_count];
                        for (uint32_t i = 0; i < primitive_count; ++i) {
                            instanceData[i] = {
                                .accelerationStructureReference = id_index_map[nodes[i].blas_va * 8 - 0x80],
                            };
                            std::memcpy(&instanceData[i].transform, &nodes[i].otw_matrix, sizeof(nodes[i].otw_matrix));
                        }

                        geometry.geometry.instances = {
                            .sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR,
                            .arrayOfPointers = false,
                        };
                        break;
                    }
                    default:
                        std::cerr << "ERROR: Invalid RRA file (Acceleration structure out of bounds)!\n";
                        return {};
                }

                BVHBuildInfo info = {
                    .geometryInfo = {
                    	.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR,
                    	.type = metadata->bvh_type == RRABvhType::BLAS
									? VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR
									: VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR,
                    	.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR,
                    	.geometryCount = 1,
                    	/* pGeometries is set later, to avoid pointer changes due to std::vector reallocation */
                    },
                    .rangeInfo = {
                    	.primitiveCount = primitive_count,
                    },
                    .geometry = geometry,
                    .fileVA = virtual_address,
                };
                if (instanceData)
                    info.instanceData = instanceData;
                if (vertexData)
                    info.vertexData = vertexData;
                result.buildInfos.push_back(info);

                delete[] header_data;
                delete[] data;
            }
        RDF_CHUNK_FOREACH_END

        /*
         * Sort the BVHs according to bottom-level vs. top-level. This ensures all bottom-level ASs are built by the
         * time the top-level ASs are built.
         */
        std::stable_sort(result.buildInfos.begin(), result.buildInfos.end(), [](const auto& a, const auto& b) {
            if (a.geometryInfo.type == VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR &&
                b.geometryInfo.type == VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR)
                return true;
            return false;
        });
        return result;
    }

} // namespace rtreplay

#endif // RRAPARSER_HPP
